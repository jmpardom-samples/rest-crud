package org.jmpardom;

import java.util.Arrays;
import java.util.concurrent.Executor;

import org.apache.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * Spring boot starter class.
 * 
 * @author jmpardom
 */
@EnableAsync
@SpringBootApplication
public class Application extends AsyncConfigurerSupport {
    
    private static final Logger LOGGER = Logger.getLogger(Application.class);
    
    /**
     * Main Spring boot method
     * 
     * @param args {@link String}[]
     */
    public static void main(
        final String[] args){
        LOGGER.info("Starting application...");
        SpringApplication.run(Application.class, args);
    }
    
    /**
     * Command line runner bean
     * 
     * @param ctx {@link ApplicationContext}
     * @return {@link CommandLineRunner}
     */
    @Bean
    public CommandLineRunner commandLineRunner(
        final ApplicationContext ctx){
        
        return args -> {
            
            LOGGER.debug("Let's inspect the beans provided by Spring Boot:");
            
            String[] beanNames = ctx.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            for (String beanName : beanNames) {
                LOGGER.debug(beanName);
            }
        };
    }
    
    /**
     * Asynchronous executor
     * 
     * @return {@link Executor}
     */
    @Override
    public Executor getAsyncExecutor(){
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(2);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("rest-sample-");
        executor.initialize();
        return executor;
    }
    
}
