package org.jmpardom.common.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.jmpardom.common.component.ErrorManager;
import org.jmpardom.common.exception.DefaultErrorManagerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * Extends Spring boot ErrorController.
 * 
 * @author jmpardom
 */
@RestController
public class CustomErrorController implements ErrorController {
    
    private static final String MESSAGE = "message";
    private static final Logger LOGGER = Logger.getLogger(CustomErrorController.class);
    private static final String PATH = "/error";
    
    @Autowired
    protected ErrorManager errorManager;
    
    @Autowired
    private ErrorAttributes errorAttributes;
    
    /**
     * Hndles errors.
     * 
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     */
    @RequestMapping(value = PATH)
    void error(
        final HttpServletRequest request,
        final HttpServletResponse response){
        
        Map<String, Object> attributes = getErrorAttributes(request, false);
        
        StringBuilder errorMessage = buildLogErrrorMessage(attributes);
        LOGGER.error("Redirected to error controller ".concat(getErrorPath()));
        LOGGER.error(errorMessage.toString());
        throw new DefaultErrorManagerException(attributes.get(CustomErrorController.MESSAGE).toString());
    }
    
    /**
     * Gets the error path.
     * 
     * @return {@link String}
     */
    @Override
    public String getErrorPath(){
        return PATH;
    }
    
    /**
     * Builds the log error message
     * 
     * @param attributes {@link Map} of {@link String}, {@link String}
     * @return {@link StringBuilder}
     */
    private StringBuilder buildLogErrrorMessage(
        final Map<String, Object> attributes){
        
        StringBuilder errorMessage = new StringBuilder();
        
        errorMessage.append("timestamp=");
        errorMessage.append(getAttributeWhenNotNull(attributes, "timestamp"));
        errorMessage.append("status=");
        errorMessage.append(getAttributeWhenNotNull(attributes, "status"));
        errorMessage.append("message=");
        errorMessage.append(getAttributeWhenNotNull(attributes, MESSAGE));
        errorMessage.append("path=");
        errorMessage.append(getAttributeWhenNotNull(attributes, "path"));
        
        return errorMessage;
    }
    
    /**
     * Gets the error attributes
     * 
     * @param request HttpServletRequestWrapper
     * @param includeStackTrace boolean
     * @return {@link Map} of {@link String}, {@link String}
     */
    private Map<String, Object> getErrorAttributes(
        final HttpServletRequest request,
        final boolean includeStackTrace){
        
        RequestAttributes requestAttributes = new ServletRequestAttributes(request);
        return errorAttributes.getErrorAttributes(requestAttributes, includeStackTrace);
    }
    
    /**
     * Gets the attribute message or a default message when it is null.
     * 
     * @param attributes {@link Map} of {@link String}, {@link Object}
     * @param attribute {@link String}
     * @return {@link String}
     */
    private String getAttributeWhenNotNull(
        final Map<String, Object> attributes,
        final String attribute){
        
        if (attributes.get(attribute) == null) {
            return "No available data for ".concat(attribute);
        }
        return attributes.get(attribute).toString();
    }
}
