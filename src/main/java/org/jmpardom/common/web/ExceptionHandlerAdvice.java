package org.jmpardom.common.web;

import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.log4j.Logger;
import org.jmpardom.common.component.ErrorManager;
import org.jmpardom.common.exception.InvalidParameterException;
import org.jmpardom.common.exception.NoResultsFoundException;
import org.jmpardom.common.model.ModelError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Centralized exception handler
 * 
 * @author jmpardom
 */
@ControllerAdvice
public class ExceptionHandlerAdvice {
    
    private static final Logger LOGGER = Logger.getLogger(ExceptionHandlerAdvice.class);
    
    @Autowired
    private ErrorManager errorManager;
    
    /**
     * Handles {@link Exception}
     * 
     * @param e {@link Exception}
     * @return {@link ResponseEntity} of {@link ModelError}
     */
    @ResponseBody
    @ExceptionHandler({ Exception.class })
    public final ResponseEntity<ModelError> handleControllerException(
        final Exception e){
        
        Exception currentException = e;
        
        LOGGER.error(e.getMessage(), e);
        ModelError modelError = new ModelError();
        
        if (currentException instanceof BindException) {
            currentException = errorManager.overrideSpringValidationResult(((BindException) e).getBindingResult());
        }
        if (currentException instanceof MethodArgumentNotValidException) {
            currentException = errorManager
                    .overrideSpringValidationResult(((MethodArgumentNotValidException) e).getBindingResult());
        }
        if (currentException instanceof ExecutionException) {
            Throwable t = ((ExecutionException) currentException).getCause();
            if (t instanceof InvalidParameterException) {
                currentException = new InvalidParameterException(t.getMessage());
            }
            if (t instanceof NoResultsFoundException) {
                currentException = new NoResultsFoundException(t.getMessage());
            }
        }
        
        Map<String, Object> errorMap = errorManager.buildResponse(currentException);
        modelError.setErrors(errorManager.getErrors(errorMap));
        
        return new ResponseEntity<>(modelError, errorManager.getStatusCode(errorMap));
        
    }
}
