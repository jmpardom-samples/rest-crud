package org.jmpardom.common.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan({ "org.jmpardom.common" })
@PropertySource("classpath:application-error.properties")
public class ErrorManagerConfiguration {

}
