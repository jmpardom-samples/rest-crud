package org.jmpardom.common.enumerated;

/**
 * Enumerated class with the response error levels.
 * 
 * @author jmpardom
 */
public enum EnumErrorLevels {
    
    ERROR,
    FATAL,
    INFO,
    WARNING;
    
}
