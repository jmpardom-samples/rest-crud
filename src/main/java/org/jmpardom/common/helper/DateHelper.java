package org.jmpardom.common.helper;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Dates static helper.
 *
 * @author jmpardom
 */
public class DateHelper {
    
    /** The Constant FORMAT_DEFAULT_DDMMYY. */
    public static final String FORMAT_DEFAULT_YYYYMMDD = "yyyy-MM-dd";
    
    /** The cal. */
    private static Calendar cal;
    
    /**
     * Instantiates a new helper date.
     */
    public DateHelper() {
        cal = Calendar.getInstance();
    }
    
    /**
     * Parses a {@link java.util.Date} in to an specified format to a date as
     * {@link String}.
     *
     * @param date {@link java.util.Date}
     * @param format {@link String}
     * @return {@link String}
     */
    public static String parseToDateAsString(
        final java.util.Date date,
        final String format){
        if (date == null || format == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }
    
    /**
     * Parses a date as {@link String} to {@link java.sql.Date} in the specified
     * format.
     *
     * @param date {@link String}
     * @param format {@link String}
     * @return the java.sql. date
     * @return{@link java.sql.Date sqlDate}
     */
    public static java.sql.Date parseToJavaSqlDate(
        final String date,
        final String format){
        DateHelper fb = new DateHelper();
        DateHelper.setDate(date, format);
        return new java.sql.Date((fb.getCalendar().getTime()).getTime());
    }
    
    /**
     * Sets the date.
     *
     * @param date {@link String}
     * @param format {@link String}
     */
    private static void setDate(
        final String date,
        final String format){
        setDate(date, format, true);
    }
    
    /**
     * Sets the date.
     *
     * @param date {@link String}
     * @param formt {@link String}
     * @param lenient boolean
     */
    private static void setDate(
        final String date,
        final String formt,
        final boolean lenient){
        
        SimpleDateFormat formatter = new SimpleDateFormat(formt);
        formatter.setLenient(lenient);
        ParsePosition pos = new ParsePosition(0);
        setDate(formatter.parse(date, pos));
    }
    
    /**
     * Sets the date.
     *
     * @param date {@link java.util.Date}
     */
    private static void setDate(
        final java.util.Date date){
        instanceCalendar(date);
    }
    
    /**
     * Instance calendar.
     *
     * @param date {@link java.util.Date}
     */
    private static void instanceCalendar(
        final java.util.Date date){
        cal = Calendar.getInstance();
        cal.setTime(date);
    }
    
    /**
     * Gets the calendar.
     *
     * @return {@link java.util.Calendar}
     */
    private Calendar getCalendar(){
        return DateHelper.cal;
    }
}
