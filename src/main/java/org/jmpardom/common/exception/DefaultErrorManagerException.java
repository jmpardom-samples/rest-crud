package org.jmpardom.common.exception;

public class DefaultErrorManagerException extends RuntimeException {

	private static final long serialVersionUID = 856966938657209024L;

	/**
	 * Constructor
	 */
	public DefaultErrorManagerException(final String message) {
		super(message);
	}
}
