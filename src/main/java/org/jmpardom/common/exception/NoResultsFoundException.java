package org.jmpardom.common.exception;

/**
 * Custom no results found in exception.
 * 
 * @author jmpardom
 */
public class NoResultsFoundException extends RuntimeException
{
    
    private static final long serialVersionUID = -5278005471979069631L;
    
    /**
     * Constructor.
     * 
     * @param string {@link String}
     */
    public NoResultsFoundException(final String message)
    {
	super(message);
    }
}
