package org.jmpardom.common.exception;

/**
 * Custom unknown error exception (default).
 * 
 * @author jmpardom
 */
public class UnknownTechnicalException extends RuntimeException
{
    
    private static final long serialVersionUID = -5278005471979069631L;
    
    /**
     * Constructor.
     * 
     * @param string {@link String}
     */
    public UnknownTechnicalException(final String message)
    {
	super(message);
    }
}
