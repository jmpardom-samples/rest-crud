package org.jmpardom.restsample.web;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.jmpardom.restsample.model.UserModel;
import org.jmpardom.restsample.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Notes REST controller.
 * 
 * @author jmpardom
 *         
 */
@RestController
public class UserRestController {
    
    private static final Logger LOGGER = Logger.getLogger(UserRestController.class);
    
    @Autowired
    private UserService userService;
    
    /**
     * Gets all users.
     * 
     * @return {@link ResponseEntity} of {@link List} of {@link UserModel}
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @GetMapping("/server/api/user")
    public ResponseEntity<List<UserModel>> getAllUsers() throws InterruptedException, ExecutionException{
        
        LOGGER.info("Getting users information...");
        Future<List<UserModel>> response = userService.getAllUsers();
        
        return new ResponseEntity<List<UserModel>>(response.get(), HttpStatus.OK);
        
    }
    
    /**
     * Gets all users.
     * 
     * @param id {@link String}
     * @return {@link ResponseEntity} of {@link UserModel}
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @ResponseBody
    @GetMapping("/server/api/user/{id}")
    public ResponseEntity<UserModel> getUser(
        @PathVariable(value = "id", required = true) final String id) throws InterruptedException, ExecutionException{
        
        LOGGER.info("Getting user information...");
        Future<UserModel> response = userService.getUser(id);
        return new ResponseEntity<UserModel>(response.get(), HttpStatus.OK);
        
    }
    
    /**
     * Adds a user.
     * 
     * @param request {@link UserModel}
     * @return {@link ResponseEntity} of {@link UserModel}
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @ResponseBody
    @PostMapping("/server/api/user")
    public ResponseEntity<UserModel> addUser(
        @Valid @RequestBody final UserModel request) throws InterruptedException, ExecutionException{
        
        LOGGER.info("Adding note...");
        Future<UserModel> response = userService.addUser(request);
        return new ResponseEntity<UserModel>(response.get(), HttpStatus.CREATED);
        
    }
    
    /**
     * Updates a user.
     * 
     * @param request {@link UserModel}
     * @return {@link ResponseEntity} of {@link UserModel}
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @ResponseBody
    @PutMapping("/server/api/user")
    public ResponseEntity<UserModel> updateUser(
        @Valid @RequestBody final UserModel request) throws InterruptedException, ExecutionException{
        
        LOGGER.info("Adding note...");
        Future<UserModel> response = userService.updateUser(request);
        return new ResponseEntity<UserModel>(response.get(), HttpStatus.OK);
        
    }
    
    /**
     * Deletes a user by ID.
     * 
     * @param id {@link String}
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @DeleteMapping("/server/api/user/{id}")
    public ResponseEntity<UserModel> deleteUser(
        @PathVariable(value = "id", required = true) final String id) throws InterruptedException, ExecutionException{
        
        LOGGER.info("Getting user information...");
        Future<UserModel> response = userService.deleteUser(id);
        return new ResponseEntity<UserModel>(response.get(), HttpStatus.NO_CONTENT);
    }
}
