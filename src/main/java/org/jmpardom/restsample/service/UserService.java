package org.jmpardom.restsample.service;

import java.util.List;
import java.util.concurrent.Future;

import org.jmpardom.restsample.model.UserModel;

/**
 * Note service interface.
 * 
 * @author jmpardom.
 */
public interface UserService {
    
    /**
     * Gets all users
     * 
     * @return {@link List} of {@link UserModel}
     */
    Future<List<UserModel>> getAllUsers();
    
    /**
     * Get a user by ID
     * 
     * @param id {@link String}
     * @return {@link UserModel}
     */
    Future<UserModel> getUser(
        final String id);
        
    /**
     * Adds a user by ID
     * 
     * @param request {@link UserModelBase}
     * @return {@link UserModel}
     */
    Future<UserModel> addUser(
        final UserModel request);
        
    /**
     * Updates a user
     * 
     * @param request {@link UserModel}
     * @return {@link UserModel}
     */
    Future<UserModel> updateUser(
        final UserModel request);
        
    /**
     * Deletes a user
     * 
     * @param id {@link String}
     * @return {@link UserModel}
     */
    Future<UserModel> deleteUser(
        final String id);
        
}
