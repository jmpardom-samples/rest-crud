package org.jmpardom.restsample.service.impl;

import java.util.List;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.jmpardom.common.exception.InvalidParameterException;
import org.jmpardom.common.exception.NoResultsFoundException;
import org.jmpardom.restsample.component.UserAdapter;
import org.jmpardom.restsample.entity.UserInfoEntity;
import org.jmpardom.restsample.model.UserModel;
import org.jmpardom.restsample.repository.UserRepository;
import org.jmpardom.restsample.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * User service implementation.
 * 
 * @author jmpardom.
 */
@Service
@Transactional(transactionManager = "transactionManager")
public class UserServiceImpl implements UserService {
    
    private static final Logger LOGGER = Logger.getLogger(UserServiceImpl.class);
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private UserAdapter userAdapter;
    
    /**
     * Gets all users
     * 
     * @return {@link List} of {@link UserModel}
     */
    @Async
    @Override
    public Future<List<UserModel>> getAllUsers(){
        
        Iterable<UserInfoEntity> entities = userRepository.findAll();
        if (entities == null || entities.spliterator().getExactSizeIfKnown() < 1) {
            String message = "There are not users in database";
            LOGGER.warn(message);
            throw new NoResultsFoundException(message);
        }
        return new AsyncResult<List<UserModel>>(userAdapter.adaptEntitiesToModel(entities));
    }
    
    /**
     * Get a user by ID
     * 
     * @param id {@link String}
     * @return {@link UserModel}
     */
    @Async
    @Override
    public Future<UserModel> getUser(
        final String id){
        
        UserInfoEntity entity = userRepository.findOne(Integer.parseInt(id));
        
        if (entity == null) {
            String message = "User not found | with ID = ".concat(id);
            LOGGER.warn(message);
            throw new InvalidParameterException(message);
        }
        return new AsyncResult<UserModel>(userAdapter.adaptEntityToModel(entity));
    }
    
    /**
     * Adds a user by ID
     * 
     * @param model {@link UserModel}
     * @return {@link UserModel}
     */
    @Async
    @Override
    public Future<UserModel> addUser(
        final UserModel model){
        LOGGER.info("Identity mode un DB is active. Ignoring JSON field ID");
        UserInfoEntity entity = saveOrUpdateUser(model);
        return new AsyncResult<UserModel>(userAdapter.adaptEntityToModel(entity));
        
    }
    
    /**
     * Updates a user
     * 
     * @param model {@link UserModel}
     * @return {@link UserModel}
     */
    @Async
    @Override
    public Future<UserModel> updateUser(
        final UserModel model){
        
        UserInfoEntity entity = userRepository.findOne(Integer.parseInt(model.getId()));
        if (entity == null) {
            String message = "User to update not found | with ID = ".concat(model.getId());
            LOGGER.warn(message);
            throw new InvalidParameterException(message);
        }
        entity = saveOrUpdateUser(model);
        return new AsyncResult<UserModel>(userAdapter.adaptEntityToModel(entity));
    }
    
    /**
     * Deletes a user
     * 
     * @param id {@link String}
     * @return {@link UserModel}
     */
    @Async
    @Override
    public Future<UserModel> deleteUser(
        final String id){
        
        UserInfoEntity entity = userRepository.findOne(Integer.parseInt(id));
        if (entity == null) {
            String message = "User to delete not found | with ID = ".concat(id);
            LOGGER.warn(message);
            throw new InvalidParameterException(message);
        }
        userRepository.delete(Integer.parseInt(id));
        return new AsyncResult<UserModel>(new UserModel());
    }
    
    /**
     * Saves or updates a user into BD.
     * 
     * @param userModel {@link UserModel}
     * @return {@link UserInfoEntity}
     */
    private UserInfoEntity saveOrUpdateUser(
        final UserModel userModel){
        UserInfoEntity entity = userAdapter.adaptModelToEntity(userModel);
        return userRepository.save(entity);
    }
    
}
