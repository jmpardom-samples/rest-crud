package org.jmpardom.restsample.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * User
 * 
 * @author jmpardom
 *         
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "name", "birthdate" })
public class UserModel implements Serializable {
    
    private static final long serialVersionUID = -2527290581623881319L;
    
    @NotNull(message = "User id is mandatory.")
    private String id;
    
    @NotNull(message = "User name is mandatory")
    @Size(max = 100, min = 1, message = "Name must have length between 1..100 characters")
    private String name;
    
    @Pattern(regexp = "^((19|20)\\d\\d)-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])",
            message = "Invalid date format, please use yyyy-MM-dd format")
    private String birthdate;
    
    /**
     * Gets the ID
     * 
     * @return {@link String}
     */
    public String getId(){
        return id;
    }
    
    /**
     * Sets the ID
     * 
     * @param id {@link String}
     */
    public void setId(
        final String id){
        this.id = id;
    }
    
    /**
     * Gets the name
     * 
     * @return {@link String}
     */
    public String getName(){
        return name;
    }
    
    /**
     * Sets the name
     * 
     * @param name {@link String}
     */
    public void setName(
        final String name){
        this.name = name;
    }
    
    /**
     * Gets the birthdate
     * 
     * @param name {@link String}
     */
    public String getBirthdate(){
        return birthdate;
    }
    
    /**
     * Sets the birthdate
     * 
     * @param name {@link String}
     */
    public void setBirthdate(
        final String birthdate){
        this.birthdate = birthdate;
    }
}
