package org.jmpardom.restsample.configuration;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Database configuration
 * 
 * @author jmpardom
 *         
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = { "org.jmpardom.*.repository" })
public class DatabaseConfiguration {
    
    /**
     * Datasource
     * 
     * @return {@link DataSource}
     */
    @Bean
    public DataSource dataSource(){
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        EmbeddedDatabase db = builder.setType(EmbeddedDatabaseType.H2)
                .addScript("sql/delete.sql")
                .addScript("sql/create.sql")
                .addScript("sql/insert.sql")
                .build();
        return db;
    }
    
    @Autowired
    private DataSource dataSource;
    
    @Value("${app.hibernate.dialect:org.hibernate.dialect.DB2Dialect}")
    private String hibernateDialec;
    
    @Value("${app.hibernate.show_sql:false}")
    private String hibernateShowSql;
    
    /**
     * Configuration of the LocalContainerEntityManagerFactoryBean
     * 
     * @return {@link LocalContainerEntityManagerFactoryBean}
     */
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(){
        
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan(new String[] { "org.jmpardom.*.entity" });
        em.setJpaVendorAdapter(jpaVendorAdapter());
        
        Properties jpaProperties = new Properties();
        jpaProperties.put("hibernate.dialect", hibernateDialec);
        jpaProperties.put("hibernate.show_sql", Boolean.parseBoolean(hibernateShowSql));
        
        em.setJpaProperties(jpaProperties);
        return em;
    }
    
    /**
     * Transaction manager
     * 
     * @param emf {@link EntityManagerFactory}
     * @return {@link JpaTransactionManager}
     */
    @Bean
    public JpaTransactionManager transactionManager(
        EntityManagerFactory emf){
        return new JpaTransactionManager(emf);
    }
    
    /**
     * Configures the JpaVendorAdapter specified
     * 
     * @return {@link JpaVendorAdapter}
     */
    private JpaVendorAdapter jpaVendorAdapter(){
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setDatabase(Database.H2);
        vendorAdapter.setDatabasePlatform("H2");
        vendorAdapter.setShowSql(Boolean.parseBoolean(hibernateShowSql));
        return vendorAdapter;
    }
    
    /**
     * Configures the PlatformTransactionManager
     * 
     * @param emf {@link EntityManagerFactory}
     * @return {@link JpaTransactionManager}
     */
    @Bean(name = "transactionManager")
    public JpaTransactionManager jpaTransactionManager(
        EntityManagerFactory emf){
        
        JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
        
        HibernateJpaDialect jpaDialect = new HibernateJpaDialect();
        jpaTransactionManager.setJpaDialect(jpaDialect);
        jpaTransactionManager.setEntityManagerFactory(emf);
        
        return jpaTransactionManager;
    }
}
