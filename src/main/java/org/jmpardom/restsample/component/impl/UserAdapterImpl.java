package org.jmpardom.restsample.component.impl;

import java.util.ArrayList;
import java.util.List;

import org.jmpardom.common.helper.DateHelper;
import org.jmpardom.restsample.component.UserAdapter;
import org.jmpardom.restsample.entity.UserInfoEntity;
import org.jmpardom.restsample.model.UserModel;
import org.springframework.stereotype.Component;

/**
 * User Adapter implementation.
 * 
 * @author jmpardom.
 */
@Component
public class UserAdapterImpl implements UserAdapter {
    
    /**
     * Adapts from user model to user entity
     * 
     * @param model {@link UserModel}
     * @return {@link UserInfoEntity}
     */
    @Override
    public UserInfoEntity adaptModelToEntity(
        final UserModel model){
        
        UserInfoEntity entity = new UserInfoEntity();
        if (model.getId() != null) {
            entity.setId(Integer.parseInt(model.getId()));
        }
        entity.setName(model.getName());
        entity.setBirthdate(DateHelper.parseToJavaSqlDate(model.getBirthdate(), DateHelper.FORMAT_DEFAULT_YYYYMMDD));
        return entity;
    }
    
    /**
     * Adapts from user entity to user model
     * 
     * @param entity {@link UserInfoEntity}
     * @return {@link UserModel}
     */
    @Override
    public UserModel adaptEntityToModel(
        final UserInfoEntity entity){
        
        UserModel model = new UserModel();
        model.setId((new StringBuilder()).append(entity.getId()).toString());
        model.setName(entity.getName());
        model.setBirthdate(
                DateHelper.parseToDateAsString(entity.getBirthdate(), DateHelper.FORMAT_DEFAULT_YYYYMMDD));
        return model;
    }
    
    /**
     * Adapts from entties to a list of models
     * 
     * @param entities {@link List} of {@link UserInfoEntity}
     * @return {@link List} of {@link UserModel}
     */
    @Override
    public List<UserModel> adaptEntitiesToModel(
        final Iterable<UserInfoEntity> entities){
        
        List<UserModel> models = new ArrayList<UserModel>();
        
        for (UserInfoEntity entity : entities) {
            models.add(adaptEntityToModel(entity));
        }
        
        return models;
    }
    
}
