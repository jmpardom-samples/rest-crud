package org.jmpardom.restsample.component;

import java.util.List;

import org.jmpardom.restsample.entity.UserInfoEntity;
import org.jmpardom.restsample.model.UserModel;

/**
 * User adapter interface.
 * 
 * @author jmpardom.
 */
public interface UserAdapter {
    
    /**
     * Adapts from user model to user entity
     * 
     * @param userModel {@link UserModel}
     * @return {@link UserInfoEntity}
     */
    UserInfoEntity adaptModelToEntity(
        final UserModel userModel);
        
    /**
     * Adapts from user entity to user model
     * 
     * @param entity {@link UserInfoEntity}
     * @return {@link UserModel}
     */
    UserModel adaptEntityToModel(
        final UserInfoEntity entity);
        
    /**
     * Adapts from entties to a list of models
     * 
     * @param entities {@link List} of {@link UserInfoEntity}
     * @return {@link List} of {@link UserModel}
     */
    List<UserModel> adaptEntitiesToModel(
        final Iterable<UserInfoEntity> entities);
        
}
