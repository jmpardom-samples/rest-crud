package org.jmpardom.restsample.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * User information entity
 * 
 * @author jmpardom
 *         
 */

@Entity
@Table(name = "USERINFO")
public class UserInfoEntity implements Serializable {
    
    private static final long serialVersionUID = -2527290581623881319L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;
    
    @Column(name = "NAME")
    private String name;
    
    @Column(name = "BIRTHDATE")
    private Date birthdate;
    
    /**
     * Gets the ID
     * 
     * @return {@link Integer}
     */
    public Integer getId(){
        return id;
    }
    
    /**
     * Sets the ID
     * 
     * @param id {@link Integer}
     */
    public void setId(
        final Integer id){
        this.id = id;
    }
    
    /**
     * Gets the name
     * 
     * @return {@link String}
     */
    public String getName(){
        return name;
    }
    
    /**
     * Sets the name
     * 
     * @param name {@link String}
     */
    public void setName(
        final String name){
        this.name = name;
    }
    
    /**
     * Gets the birthdate
     * 
     * @param name {@link Date}
     */
    public Date getBirthdate(){
        if (birthdate == null) {
            return null;
        }
        return new Date(birthdate.getTime());
    }
    
    /**
     * Sets the birthdate
     * 
     * @param name {@link Date}
     */
    public void setBirthdate(
        final Date birthdate){
        
        this.birthdate = birthdate;
    }
}
