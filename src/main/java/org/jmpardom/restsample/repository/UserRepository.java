package org.jmpardom.restsample.repository;

import org.jmpardom.restsample.entity.UserInfoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * User repository interface.
 * 
 * @author jmpardom
 */
@Repository
public interface UserRepository extends CrudRepository<UserInfoEntity, Integer> {

}
