-- ----------------------------
-- User information
-- ----------------------------
CREATE TABLE IF NOT EXISTS USERINFO (
    ID  		BIGINT			AUTO_INCREMENT	NOT NULL,
    NAME 		NVARCHAR(100)   				NOT NULL,
    BIRTHDATE 	DATE,
    PRIMARY KEY (ID)
) ;


