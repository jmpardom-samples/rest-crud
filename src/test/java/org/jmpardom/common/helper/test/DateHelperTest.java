package org.jmpardom.common.helper.test;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.jmpardom.common.helper.DateHelper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * IntegrationTest for DeteHelper.
 * 
 * @author jmpardom
 */
public class DateHelperTest {
    
    protected static final String DATE_2016_04_20 = "2016-04-20";
    
    protected Calendar cal;
    protected java.sql.Date sqlDate;
    protected Timestamp timestamp;
    
    /**
     * Sets the up the test environment.
     * 
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception{
        cal = Calendar.getInstance();
        sqlDate = new java.sql.Date(cal.getTime().getTime());
        timestamp = new Timestamp(sqlDate.getTime());
    }
    
    /**
     * Test for {@link DateHelper#parseToJavaSqlDate(String, String)}
     * 
     * @throws ParseException pe
     */
    @Test
    public void parseToJavaSqlDateTest() throws ParseException{
        assertEquals(DateHelper.parseToJavaSqlDate(DATE_2016_04_20, DateHelper.FORMAT_DEFAULT_YYYYMMDD),
                getSqlDate(DATE_2016_04_20));
                
    }
    
    /**
     * Test for {@link DateHelper#parseToDateAsString(java.util.Date, String)}
     * 
     * @throws ParseException pe
     */
    @Test
    public void parseToDateAsStringTest(){
        assertEquals(DateHelper.parseToDateAsString(getDate(DATE_2016_04_20), DateHelper.FORMAT_DEFAULT_YYYYMMDD),
                DATE_2016_04_20);
                
    }
    
    /**
     * Test for {@link DateHelper#parseToDateAsString(java.util.Date, String)}
     * 
     * @throws ParseException pe
     */
    @Test
    public void parseToDateAsStringWithNullFormatTest(){
        Assert.assertNull(DateHelper.parseToDateAsString(getDate(DATE_2016_04_20), null));
        
    }
    
    /**
     * Test for {@link DateHelper#parseToDateAsString(java.util.Date, String)}
     * 
     * @throws ParseException pe
     */
    @Test
    public void parseToDateAsStringWithNullDateTest(){
        Assert.assertNull(DateHelper.parseToDateAsString(null, DateHelper.FORMAT_DEFAULT_YYYYMMDD));
        
    }
    
    /**
     * Test for {@link DateHelper#parseToDateAsString(java.sql.Date, String)}
     * 
     * @throws ParseException pe
     */
    @Test
    public void dateToString() throws ParseException{
        assertEquals(DateHelper.parseToDateAsString(getSqlDate(DATE_2016_04_20), DateHelper.FORMAT_DEFAULT_YYYYMMDD),
                DATE_2016_04_20);
                
    }
    
    /**
     * Gets the parsed {@link java.util.Date}.
     *
     * @param date String
     * @return {@link java.util.Date}
     */
    private Date getDate(
        String date){
        cal = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat(DateHelper.FORMAT_DEFAULT_YYYYMMDD);
        ParsePosition pos = new ParsePosition(0);
        return formatter.parse(date, pos);
    }
    
    /**
     * Gets the parsed {@link java.sql.Date}.
     *
     * @param date String
     * @return {@link java.sql.Date}
     * @throws ParseException pe
     */
    private java.sql.Date getSqlDate(
        String date) throws ParseException{
        SimpleDateFormat format = new SimpleDateFormat(DateHelper.FORMAT_DEFAULT_YYYYMMDD);
        Date parsed = format.parse(date);
        java.sql.Date sql = new java.sql.Date(parsed.getTime());
        return sql;
    }
}
