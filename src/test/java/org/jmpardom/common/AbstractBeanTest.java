package org.jmpardom.common;

import org.junit.Before;
import org.meanbean.test.BeanTester;

/**
 * Abstract Integration controller tests with common features.
 * 
 * @author jmpardom
 */
public abstract class AbstractBeanTest {
    
    protected BeanTester beanTester;
    
    /**
     * Sets up test environment.
     * 
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception{
        beanTester = new BeanTester();
        
    }
}
