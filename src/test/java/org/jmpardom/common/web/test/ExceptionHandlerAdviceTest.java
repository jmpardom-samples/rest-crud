package org.jmpardom.common.web.test;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.jmpardom.common.AbstractUnitaryTest;
import org.jmpardom.common.component.ErrorManager;
import org.jmpardom.common.exception.InvalidParameterException;
import org.jmpardom.common.exception.NoResultsFoundException;
import org.jmpardom.common.exception.UnknownTechnicalException;
import org.jmpardom.common.model.Error;
import org.jmpardom.common.web.ExceptionHandlerAdvice;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;

/**
 * Mocked jUnit test for {@link ExceptionHandlerAdvice}
 * 
 * @author jmpardom
 */
public class ExceptionHandlerAdviceTest extends AbstractUnitaryTest {
    
    @InjectMocks
    private ExceptionHandlerAdvice exceptionHandlerAdvice;
    
    @Mock
    private ErrorManager errorManager;
    
    @Mock
    private BindException bindException;
    
    @Mock
    private MethodArgumentNotValidException methodArgumentNotValidException;
    
    private ExecutionException executionExceptionNoResults;
    private ExecutionException executionExceptionInvalidParameter;
    
    private Map<String, Object> errorMap;
    private List<Error> errors;
    
    /**
     * Sets up test environment.
     * 
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception{
        super.setUp();
        executionExceptionNoResults = new ExecutionException(new NoResultsFoundException(ANY_STRING));
        executionExceptionInvalidParameter = new ExecutionException(new InvalidParameterException(ANY_STRING));
    }
    
    /**
     * Tests {@link ExceptionHandlerAdvice#handleControllerException(Exception)}
     */
    @Test
    public void handleControllerExceptionTest(){
        
        Mockito.when(errorManager.buildResponse(Mockito.any(Exception.class))).thenReturn(errorMap);
        Mockito.when(errorManager.getErrors(Mockito.anyMapOf(String.class, Object.class))).thenReturn(errors);
        Mockito.when(errorManager.getStatusCode(Mockito.anyMapOf(String.class, Object.class)))
                .thenReturn(HttpStatus.INTERNAL_SERVER_ERROR);
                
        exceptionHandlerAdvice.handleControllerException(new UnknownTechnicalException(ANY_STRING));
        
        Mockito.verify(errorManager).buildResponse(Mockito.any(Exception.class));
        Mockito.verify(errorManager).getErrors(Mockito.anyMapOf(String.class, Object.class));
        Mockito.verify(errorManager).getStatusCode(Mockito.anyMapOf(String.class, Object.class));
        Mockito.verifyNoMoreInteractions(errorManager);
    }
    
    /**
     * Tests {@link ExceptionHandlerAdvice#handleControllerException(Exception)}
     */
    @Test
    public void handleControllerExceptionWithBindExceptionTest(){
        
        Mockito.when(errorManager.buildResponse(Mockito.any(Exception.class))).thenReturn(errorMap);
        Mockito.when(errorManager.getErrors(Mockito.anyMapOf(String.class, Object.class))).thenReturn(errors);
        Mockito.when(errorManager.getStatusCode(Mockito.anyMapOf(String.class, Object.class)))
                .thenReturn(HttpStatus.INTERNAL_SERVER_ERROR);
        Mockito.when(errorManager.overrideSpringValidationResult(Mockito.any(BindingResult.class)))
                .thenReturn(new InvalidParameterException(ANY_STRING));
                
        exceptionHandlerAdvice.handleControllerException(bindException);
        
        Mockito.verify(errorManager).overrideSpringValidationResult(Mockito.any(BindingResult.class));
        Mockito.verify(errorManager).buildResponse(Mockito.any(Exception.class));
        Mockito.verify(errorManager).getErrors(Mockito.anyMapOf(String.class, Object.class));
        Mockito.verify(errorManager).getStatusCode(Mockito.anyMapOf(String.class, Object.class));
        Mockito.verifyNoMoreInteractions(errorManager);
    }
    
    /**
     * Tests {@link ExceptionHandlerAdvice#handleControllerException(Exception)}
     */
    @Test
    public void handleControllerExceptionWithMethodArgumentNotValidExceptionTest(){
        
        Mockito.when(errorManager.buildResponse(Mockito.any(Exception.class))).thenReturn(errorMap);
        Mockito.when(errorManager.getErrors(Mockito.anyMapOf(String.class, Object.class))).thenReturn(errors);
        Mockito.when(errorManager.getStatusCode(Mockito.anyMapOf(String.class, Object.class)))
                .thenReturn(HttpStatus.INTERNAL_SERVER_ERROR);
        Mockito.when(errorManager.overrideSpringValidationResult(Mockito.any(BindingResult.class)))
                .thenReturn(new InvalidParameterException(ANY_STRING));
                
        exceptionHandlerAdvice.handleControllerException(methodArgumentNotValidException);
        
        Mockito.verify(errorManager).overrideSpringValidationResult(Mockito.any(BindingResult.class));
        Mockito.verify(errorManager).buildResponse(Mockito.any(Exception.class));
        Mockito.verify(errorManager).getErrors(Mockito.anyMapOf(String.class, Object.class));
        Mockito.verify(errorManager).getStatusCode(Mockito.anyMapOf(String.class, Object.class));
        Mockito.verifyNoMoreInteractions(errorManager);
    }
    
    /**
     * Tests {@link ExceptionHandlerAdvice#handleControllerException(Exception)}
     */
    @Test
    public void handleControllerExceptionWithExecutionExceptionInvalidParamsTest(){
        
        Mockito.when(errorManager.buildResponse(Mockito.any(Exception.class))).thenReturn(errorMap);
        Mockito.when(errorManager.getErrors(Mockito.anyMapOf(String.class, Object.class))).thenReturn(errors);
        Mockito.when(errorManager.getStatusCode(Mockito.anyMapOf(String.class, Object.class)))
                .thenReturn(HttpStatus.INTERNAL_SERVER_ERROR);
        Mockito.when(errorManager.overrideSpringValidationResult(Mockito.any(BindingResult.class)))
                .thenReturn(new InvalidParameterException(ANY_STRING));
                
        exceptionHandlerAdvice.handleControllerException(executionExceptionInvalidParameter);
        
        Mockito.verify(errorManager).buildResponse(Mockito.any(Exception.class));
        Mockito.verify(errorManager).getErrors(Mockito.anyMapOf(String.class, Object.class));
        Mockito.verify(errorManager).getStatusCode(Mockito.anyMapOf(String.class, Object.class));
        Mockito.verifyNoMoreInteractions(errorManager);
    }
    
    /**
     * Tests {@link ExceptionHandlerAdvice#handleControllerException(Exception)}
     */
    @Test
    public void handleControllerExceptionWithExecutionExceptionNoResultsTest(){
        
        Mockito.when(errorManager.buildResponse(Mockito.any(Exception.class))).thenReturn(errorMap);
        Mockito.when(errorManager.getErrors(Mockito.anyMapOf(String.class, Object.class))).thenReturn(errors);
        Mockito.when(errorManager.getStatusCode(Mockito.anyMapOf(String.class, Object.class)))
                .thenReturn(HttpStatus.INTERNAL_SERVER_ERROR);
        Mockito.when(errorManager.overrideSpringValidationResult(Mockito.any(BindingResult.class)))
                .thenReturn(new InvalidParameterException(ANY_STRING));
                
        exceptionHandlerAdvice.handleControllerException(executionExceptionNoResults);
        
        Mockito.verify(errorManager).buildResponse(Mockito.any(Exception.class));
        Mockito.verify(errorManager).getErrors(Mockito.anyMapOf(String.class, Object.class));
        Mockito.verify(errorManager).getStatusCode(Mockito.anyMapOf(String.class, Object.class));
        Mockito.verifyNoMoreInteractions(errorManager);
    }
}
