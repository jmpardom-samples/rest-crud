package org.jmpardom.common.web.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jmpardom.Application;
import org.jmpardom.common.web.CustomErrorController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

/**
 * Integration test for {@link CustomErrorController}
 * 
 * @author jmpardom
 */
@WithAnonymousUser
@WebAppConfiguration
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class CustomErrorControllerTest {
    
    protected static final String ANY_STRING = "any";
    protected static final String ANY_INT_AS_STRING = "1";
    
    @Autowired
    private WebApplicationContext webApplicationContext;
    
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private ErrorAttributes errorAttributes;
    
    private MockMvc mockMvc;
    
    /**
     * Sets up test environment.
     * 
     * @throws Exception e
     */
    @Before
    public void setup() throws Exception{
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        
    }
    
    /**
     * Test for {@link CustomErrorController#error()}
     * 
     * @throws Exception
     */
    @Test
    public void errorGetTest() throws Exception{
        String uri = "/error";
        mockMvc.perform(get(uri, request, response))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print());
    }
    
    /**
     * Test for {@link CustomErrorController#error()}
     * 
     * @throws Exception
     */
    @Test
    public void errorPostTest() throws Exception{
        String uri = "/error";
        mockMvc.perform(post(uri, request, response))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print());
    }
    
    /**
     * Test for {@link CustomErrorController#error()}
     * 
     * @throws Exception
     */
    @Test
    public void errorPutTest() throws Exception{
        String uri = "/error";
        mockMvc.perform(put(uri, request, response))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print());
    }
    
    /**
     * Test for {@link CustomErrorController#error()}
     * 
     * @throws Exception
     */
    @Test
    public void errorDeleteTest() throws Exception{
        String uri = "/error";
        mockMvc.perform(delete(uri, request, response))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print());
    }
    
}
