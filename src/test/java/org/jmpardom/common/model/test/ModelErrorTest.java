package org.jmpardom.common.model.test;

import org.jmpardom.common.AbstractBeanTest;
import org.jmpardom.common.model.ModelError;
import org.junit.Test;

/**
 * {@link ModelError} Test Class.
 * 
 * @author jmpardom
 *         
 */
public class ModelErrorTest extends AbstractBeanTest {
    /**
     * Test for POJO {@link ModelError}
     */
    @Test
    public void modelErrorTest(){
        beanTester.testBean(ModelError.class);
    }
}
