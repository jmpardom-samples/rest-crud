package org.jmpardom.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jmpardom.restsample.entity.UserInfoEntity;
import org.jmpardom.restsample.model.UserModel;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Abstract Integration controller tests with common features.
 * 
 * @author jmpardom
 */
@RunWith(MockitoJUnitRunner.class)
public abstract class AbstractUnitaryTest {
    
    protected static final String ANY_ENUM_ERROR_LEVEL = "ERROR";
    protected static final String ANY_HTTP_STATUS_CODE = "200";
    protected static final String ANY_STRING = "any string";
    protected static final int ANY_INT = 1;
    protected static final String ANY_INT_AS_STRING = "1";
    protected static final long ANY_LONG = 1;
    protected static final Object ANY_OBJECT = new Object();
    protected static final Date ANY_DATE = new Date();
    protected static final java.sql.Date ANY_SQL_DATE = new java.sql.Date(ANY_DATE.getTime());
    protected static final Object[] ANY_OBJECT_ARRAY = { ANY_OBJECT, ANY_OBJECT };
    protected static final String[] ANY_STRING_ARRAY = { ANY_STRING, ANY_STRING };
    protected static final String ANY_DATE_AS_STRING = "2018-01-01";
    
    /**
     * Sets up the test environment
     * 
     * @throws Exception e
     */
    @Before
    public void setUp() throws Exception{
    
    }
    
    /**
     * Creates a dummy UserModel
     * 
     * @param anyId {@link String}
     * @return {@link UserModel}
     */
    protected UserModel buildDummyUserModel(
        final String anyId){
        
        UserModel userModel = new UserModel();
        
        userModel.setBirthdate(ANY_DATE_AS_STRING);
        userModel.setId(anyId);
        userModel.setName(ANY_STRING);
        
        return userModel;
    }
    
    /**
     * Creates a dummy UserInfoEntity
     * 
     * @param anySqlDate
     *            
     * @return {@link UserModel}
     */
    protected UserInfoEntity buildDummyUserEInfoEntity(
        final java.sql.Date anySqlDate){
        
        UserInfoEntity entity = new UserInfoEntity();
        
        entity.setBirthdate(anySqlDate);
        entity.setId(ANY_INT);
        entity.setName(ANY_STRING);
        
        return entity;
    }
    
    /**
     * Builds a dummy list of entities
     * 
     * @return {@link Iterable} of {@link UserInfoEntity}
     */
    protected Iterable<UserInfoEntity> buildDummyEntitiesList(){
        
        List<UserInfoEntity> entities = new ArrayList<UserInfoEntity>();
        entities.add(buildDummyUserEInfoEntity(ANY_SQL_DATE));
        entities.add(buildDummyUserEInfoEntity(ANY_SQL_DATE));
        entities.add(buildDummyUserEInfoEntity(ANY_SQL_DATE));
        
        Iterable<UserInfoEntity> iterable = entities;
        
        return iterable;
    }
    
    /**
     * Builds a dummy list of models
     * 
     * @return {@link List} of {@link UserModel}
     */
    protected List<UserModel> buildDummyUserModels(){
        
        List<UserModel> users = new ArrayList<UserModel>();
        users.add(buildDummyUserModel(ANY_INT_AS_STRING));
        users.add(buildDummyUserModel(ANY_INT_AS_STRING));
        users.add(buildDummyUserModel(ANY_INT_AS_STRING));
        
        return users;
    }
    
}
