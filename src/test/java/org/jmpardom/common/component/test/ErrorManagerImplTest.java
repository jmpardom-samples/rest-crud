package org.jmpardom.common.component.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jmpardom.common.AbstractUnitaryTest;
import org.jmpardom.common.component.impl.ErrorManagerImpl;
import org.jmpardom.common.exception.InvalidParameterException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

/**
 * Unitary test of {@link ErrorManagerImpl}
 * 
 * @author jmpardom
 *         
 */
public class ErrorManagerImplTest extends AbstractUnitaryTest {
    
    @InjectMocks
    private ErrorManagerImpl errorManagerImpl;
    
    @Mock
    private Environment enviroment;
    
    @Mock
    private BindingResult bindingResult;
    
    private Exception e;
    private Exception eWithMessage;
    private Exception eWithCustomMessage;
    private Map<String, Object> errorMap;
    private List<ObjectError> objectErrors;
    
    /**
     * Sets up the eWithMessagetest environment.
     */
    @Before
    public void setUp(){
        e = new Exception();
        eWithMessage = new Exception(ANY_STRING);
        eWithCustomMessage = new Exception("#".concat(ANY_STRING));
        errorMap = buildDummyErrorMap();
        objectErrors = buildDummyObjectErrors();
        
    }
    
    /**
     * {@link ErrorManagerImpl#getErrors()}
     */
    @Test
    public void overrideSpringValidationResultTest(){
        
        Mockito.when(bindingResult.hasErrors()).thenReturn(true);
        Mockito.when(bindingResult.getAllErrors()).thenReturn(objectErrors);
        
        InvalidParameterException invalid = errorManagerImpl.overrideSpringValidationResult(bindingResult);
        
        assertThat(invalid).isNotNull();
        assertThat(invalid.getMessage()).isEqualTo("#ERROR");
        
        Mockito.verifyZeroInteractions(enviroment);
    }
    
    /**
     * {@link ErrorManagerImpl#getErrors()}
     */
    @Test
    public void getErrorsTest(){
        
        Object value = errorManagerImpl.getErrors(errorMap);
        
        Assert.assertTrue(value instanceof ArrayList<?>);
        Mockito.verifyZeroInteractions(enviroment);
    }
    
    /**
     * {@link ErrorManagerImpl#getStatusCode(Map)}
     */
    @Test
    public void getStatusCodeTest(){
        
        Object value = errorManagerImpl.getStatusCode(errorMap);
        
        Assert.assertTrue(value instanceof HttpStatus);
        Mockito.verifyZeroInteractions(enviroment);
    }
    
    /**
     * {@link ErrorManagerImpl#buildResponse(Exception, boolean)}
     */
    @Test
    public void buildErrorResponseTest(){
        
        Mockito.when(enviroment.getProperty(Mockito.anyString())).thenReturn(ANY_HTTP_STATUS_CODE,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL);
                
        errorManagerImpl.buildResponse(e);
        
        Mockito.verify(enviroment, Mockito.times(5)).getProperty(Mockito.anyString());
        Mockito.verifyNoMoreInteractions(enviroment);
    }
    
    /**
     * {@link ErrorManagerImpl#buildResponse(Exception, boolean)}
     */
    @Test
    public void buildErrorResponseReplacingMessageTest(){
        
        Mockito.when(enviroment.getProperty(Mockito.anyString())).thenReturn(ANY_HTTP_STATUS_CODE,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL);
                
        errorManagerImpl.buildResponse(eWithMessage);
        
        Mockito.verify(enviroment, Mockito.times(4)).getProperty(Mockito.anyString());
        Mockito.verifyNoMoreInteractions(enviroment);
    }
    
    /**
     * {@link ErrorManagerImpl#buildResponse(Exception, boolean)}
     */
    @Test
    public void buildErrorResponseReplacingCustomMessageTest(){
        
        Mockito.when(enviroment.getProperty(Mockito.anyString())).thenReturn(ANY_HTTP_STATUS_CODE,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL);
                
        errorManagerImpl.buildResponse(eWithCustomMessage);
        
        Mockito.verify(enviroment, Mockito.times(4)).getProperty(Mockito.anyString());
        Mockito.verifyNoMoreInteractions(enviroment);
    }
    
    /**
     * {@link ErrorManagerImpl#buildResponse(Exception, boolean)}
     */
    @Test
    public void buildErrorResponseWhenNotFoundCodeTest(){
        
        Mockito.when(enviroment.getProperty(Mockito.anyString())).thenReturn(null,
                null,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL);
                
        errorManagerImpl.buildResponse(e);
        
        Mockito.verify(enviroment, Mockito.times(5)).getProperty(Mockito.anyString());
        Mockito.verifyNoMoreInteractions(enviroment);
    }
    
    /**
     * {@link ErrorManagerImpl#buildResponse(Exception, boolean)}
     */
    @Test
    public void buildErrorResponseWhenNotFoundMessageTest(){
        
        Mockito.when(enviroment.getProperty(Mockito.anyString())).thenReturn(null,
                ANY_ENUM_ERROR_LEVEL,
                null,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL);
                
        errorManagerImpl.buildResponse(e);
        
        Mockito.verify(enviroment, Mockito.times(9)).getProperty(Mockito.anyString());
        Mockito.verifyNoMoreInteractions(enviroment);
    }
    
    /**
     * {@link ErrorManagerImpl#buildResponse(Exception, boolean)}
     */
    @Test
    public void buildErrorResponseWhenNotFoundLevelTest(){
        
        Mockito.when(enviroment.getProperty(Mockito.anyString())).thenReturn(null,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL,
                null,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL);
                
        errorManagerImpl.buildResponse(e);
        
        Mockito.verify(enviroment, Mockito.times(9)).getProperty(Mockito.anyString());
        Mockito.verifyNoMoreInteractions(enviroment);
    }
    
    /**
     * {@link ErrorManagerImpl#buildResponse(Exception, boolean)}
     */
    @Test
    public void buildErrorResponseWhenNotFoundDescriptionTest(){
        
        Mockito.when(enviroment.getProperty(Mockito.anyString())).thenReturn(null,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL,
                null,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL,
                ANY_ENUM_ERROR_LEVEL);
                
        errorManagerImpl.buildResponse(e);
        
        Mockito.verify(enviroment, Mockito.times(9)).getProperty(Mockito.anyString());
        Mockito.verifyNoMoreInteractions(enviroment);
    }
    
    /**
     * Builds a dummy error map.
     * 
     * @return {@link Map} of {@link String}, {@link Object},
     */
    private Map<String, Object> buildDummyErrorMap(){
        Map<String, Object> errorMap = new HashMap<String, Object>();
        
        errorMap.put("errors", new ArrayList<Error>());
        errorMap.put("status", HttpStatus.OK);
        
        return errorMap;
    }
    
    /**
     * Builds a dummy list of spring object errors
     * 
     * @return List of {@link ObjectError}
     */
    private List<ObjectError> buildDummyObjectErrors(){
        
        List<ObjectError> objectErrors = new ArrayList<ObjectError>();
        ObjectError objectError = new ObjectError(ANY_ENUM_ERROR_LEVEL, ANY_ENUM_ERROR_LEVEL);
        objectErrors.add(objectError);
        
        return objectErrors;
    }
}
