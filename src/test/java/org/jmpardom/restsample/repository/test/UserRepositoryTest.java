package org.jmpardom.restsample.repository.test;

import org.jmpardom.Application;
import org.jmpardom.common.helper.DateHelper;
import org.jmpardom.restsample.entity.UserInfoEntity;
import org.jmpardom.restsample.repository.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Test class for {@link UserRepository} implementation.
 * 
 * @author jmpardom
 */
@WebAppConfiguration
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class UserRepositoryTest {
    
    protected static final String ANY_STRING = "any";
    protected static final String ANY_DATE_AS_STRING = "2018-01-01";
    
    @Autowired
    private UserRepository userRepository;
    
    private UserInfoEntity newEntity;
    private UserInfoEntity existingEntity;
    
    /**
     * Sets up the test environment
     * 
     * @throws Exception e
     */
    @Before
    public void setUp() throws Exception{
        newEntity = buildDummyEntity(3);
        existingEntity = buildDummyEntity(2);
    }
    
    /**
     * Test for {@link UserRepository#findAll()}
     */
    @Test
    public void findAllTest(){
        
        Iterable<UserInfoEntity> entities = userRepository.findAll();
        
        Assert.assertNotNull(entities);
    }
    
    /**
     * Test for {@link UserRepository#findOne(Integer)}
     */
    @Test
    public void findOneTest(){
        
        UserInfoEntity entity = userRepository.findOne(1);
        
        Assert.assertNotNull(entity);
        Assert.assertEquals(new Integer(1), entity.getId());
    }
    
    /**
     * Test for {@link UserRepository#findOne(Integer)}
     */
    @Test
    public void findOneNotFoundTest(){
        
        UserInfoEntity entity = userRepository.findOne(11);
        
        Assert.assertNull(entity);
    }
    
    /**
     * Test for {@link UserRepository#findOne(Integer)}
     */
    @Test
    public void save(){
        
        UserInfoEntity entity = userRepository.save(newEntity);
        
        Assert.assertNotNull(entity);
        Assert.assertEquals(new Integer(3), entity.getId());
    }
    
    /**
     * Test for {@link UserRepository#save(UserInfoEntity)}
     */
    @Test
    public void update(){
        
        UserInfoEntity entity = userRepository.save(existingEntity);
        
        Assert.assertNotNull(entity);
        Assert.assertEquals(new Integer(3), entity.getId());
    }
    
    /**
     * Test for {@link UserRepository#delete(Integer)}
     */
    @Test
    public void delete(){
        
        userRepository.delete(existingEntity);
        UserInfoEntity entity = userRepository.findOne(2);
        
        Assert.assertNull(entity);
    }
    
    /**
     * Builds a dummy entity.
     * 
     * @param id {@link Integer}
     * @return
     */
    private UserInfoEntity buildDummyEntity(
        final Integer id){
        
        UserInfoEntity entity = new UserInfoEntity();
        
        entity.setBirthdate(DateHelper.parseToJavaSqlDate(ANY_DATE_AS_STRING, DateHelper.FORMAT_DEFAULT_YYYYMMDD));
        entity.setId(id);
        entity.setName(ANY_STRING);
        
        return entity;
    }
}
