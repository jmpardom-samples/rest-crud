package org.jmpardom.restsample.model.test;

import org.jmpardom.common.AbstractBeanTest;
import org.jmpardom.restsample.model.UserModel;
import org.junit.Test;

/**
 * {@link UserModel} Test Class.
 * 
 * @author jmpardom
 *         
 */
public class UserModelTest extends AbstractBeanTest {
    /**
     * Test for POJO {@link UserModel}
     */
    @Test
    public void userModelTest(){
        beanTester.testBean(UserModel.class);
    }
}
