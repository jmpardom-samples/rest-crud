package org.jmpardom.restsample.web.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import org.jmpardom.Application;
import org.jmpardom.restsample.model.UserModel;
import org.jmpardom.restsample.web.UserRestController;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Integration test for {@link UserRestController}
 * 
 * @author jmpardom
 */
@WithAnonymousUser
@WebAppConfiguration
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserRestControllerTest {
    
    protected static final String ANY_STRING = "any";
    protected static final String ANY_INT_AS_STRING = "1";
    protected static final String ANY_DATE_AS_STRING = "2018-01-01";
    
    @Autowired
    private WebApplicationContext webApplicationContext;
    
    private MockMvc mockMvc;
    private UserModel userModel;
    private UserModel invalidIdUserModel;
    private UserModel invalidNameUserModel;
    private UserModel invalidIdBirthdateUserModel;
    private String jsonUserModel;
    private String jsonInvalidIdUserModel;
    private String jsonInvalidNameUserModel;
    private String jsonInvalidIdBirthdateUserModel;
    
    @Before
    public void setup() throws Exception{
        userModel = buildDummyUserModel(ANY_INT_AS_STRING, ANY_STRING, ANY_DATE_AS_STRING);
        invalidIdUserModel = buildDummyUserModel(null, ANY_STRING, ANY_DATE_AS_STRING);
        invalidNameUserModel = buildDummyUserModel(ANY_INT_AS_STRING, null, ANY_DATE_AS_STRING);
        invalidIdBirthdateUserModel = buildDummyUserModel(ANY_INT_AS_STRING, ANY_STRING, "11-55-3000");
        ObjectMapper mapper = new ObjectMapper();
        jsonUserModel = mapper.writeValueAsString(userModel);
        jsonInvalidIdUserModel = mapper.writeValueAsString(invalidIdUserModel);
        jsonInvalidNameUserModel = mapper.writeValueAsString(invalidNameUserModel);
        jsonInvalidIdBirthdateUserModel = mapper.writeValueAsString(invalidIdBirthdateUserModel);
        
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        
    }
    
    /**
     * Test for {@link UserRestController#getAllUsers()}
     * 
     * @throws Exception
     */
    @Test
    public void notFoundCaseTest() throws Exception{
        String uri = "/any";
        mockMvc.perform(get(uri))
                .andExpect(status().isNotFound())
                .andDo(print());
    }
    
    /**
     * Test for {@link UserRestController#getAllUsers()}
     * 
     * @throws Exception
     */
    @Test
    public void getAllUsersTest() throws Exception{
        String uri = "/server/api/user";
        mockMvc.perform(get(uri))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andDo(print());
    }
    
    /**
     * Test for {@link UserRestController#getUser(String)}
     * 
     * @throws Exception
     */
    @Test
    public void getUserTest() throws Exception{
        String uri = "/server/api/user/1";
        mockMvc.perform(get(uri))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andDo(print());
    }
    
    /**
     * Test for {@link UserRestController#getUser(String)}
     * 
     * @throws Exception
     */
    @Test
    public void getUserWithWrongPathParameterTest() throws Exception{
        String uri = "/server/api/user/55";
        mockMvc.perform(get(uri))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().is4xxClientError())
                .andDo(print());
    }
    
    /**
     * Test for {@link UserRestController#addUser(UserModel)}
     * 
     * @throws Exception
     */
    @Test
    public void addUserTest() throws Exception{
        String uri = "/server/api/user";
        mockMvc.perform(post(uri)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(jsonUserModel))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print());
    }
    
    /**
     * Test for {@link UserRestController#addUser(UserModel)}
     * 
     * @throws Exception
     */
    @Test
    public void addUserWithInvalidJsonRequestIdTest() throws Exception{
        String uri = "/server/api/user";
        mockMvc.perform(post(uri)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(jsonInvalidIdUserModel))
                .andExpect(status().is4xxClientError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print());
    }
    
    /**
     * Test for {@link UserRestController#addUser(UserModel)}
     * 
     * @throws Exception
     */
    @Test
    public void addUserWithInvalidJsonRequestNameTest() throws Exception{
        String uri = "/server/api/user";
        mockMvc.perform(post(uri)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(jsonInvalidNameUserModel))
                .andExpect(status().is4xxClientError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print());
    }
    
    /**
     * Test for {@link UserRestController#addUser(UserModel)}
     * 
     * @throws Exception
     */
    @Test
    public void addUserWithInvalidJsonRequestBirthdateTest() throws Exception{
        String uri = "/server/api/user";
        mockMvc.perform(post(uri)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(jsonInvalidIdBirthdateUserModel))
                .andExpect(status().is4xxClientError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print());
    }
    
    /**
     * Test for {@link UserRestController#updateUser(UserModel)}
     * 
     * @throws Exception
     */
    @Test
    public void updateUserTest() throws Exception{
        String uri = "/server/api/user";
        mockMvc.perform(put(uri)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(jsonUserModel))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print());
    }
    
    /**
     * Test for {@link UserRestController#updateUser(UserModel)}
     * 
     * @throws Exception
     */
    @Test
    public void updateUserWithInvalidJsonRequestIdTest() throws Exception{
        String uri = "/server/api/user";
        mockMvc.perform(put(uri)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(jsonInvalidIdUserModel))
                .andExpect(status().is4xxClientError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print());
    }
    
    /**
     * Test for {@link UserRestController#updateUser(UserModel)}
     * 
     * @throws Exception
     */
    @Test
    public void updateUserWithInvalidJsonRequestNameTest() throws Exception{
        String uri = "/server/api/user";
        mockMvc.perform(put(uri)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(jsonInvalidNameUserModel))
                .andExpect(status().is4xxClientError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print());
    }
    
    /**
     * Test for {@link UserRestController#updateUser(UserModel)}
     * 
     * @throws Exception
     */
    @Test
    public void updateUserWithInvalidJsonRequestBirthdateTest() throws Exception{
        String uri = "/server/api/user";
        mockMvc.perform(put(uri)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(jsonInvalidIdBirthdateUserModel))
                .andExpect(status().is4xxClientError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print());
    }
    
    /**
     * Test for {@link UserRestController#getUser(String)}
     * 
     * @throws Exception
     */
    @Test
    public void delteUserTest() throws Exception{
        String uri = "/server/api/user/2";
        mockMvc.perform(delete(uri))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print());
    }
    
    /**
     * Creates a dummy UserModel
     * 
     * @param anyId {@link String}
     * @param anyName {@link String}
     * @param anyDate {@link String}
     * @return {@link UserModel}
     */
    private UserModel buildDummyUserModel(
        final String anyId,
        final String anyName,
        final String anyDate){
        
        UserModel userModel = new UserModel();
        
        userModel.setBirthdate(anyDate);
        userModel.setId(anyId);
        userModel.setName(anyName);
        
        return userModel;
    }
    
}
