package org.jmpardom.restsample.component.test;

import java.util.List;

import org.jmpardom.common.AbstractUnitaryTest;
import org.jmpardom.restsample.component.impl.UserAdapterImpl;
import org.jmpardom.restsample.entity.UserInfoEntity;
import org.jmpardom.restsample.model.UserModel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;

/**
 * Mocked jUnit test for {@link UserAdapterImpl}
 * 
 * @author jmpardom
 */
public class UserAdapterImplTest extends AbstractUnitaryTest {
    
    @InjectMocks
    private UserAdapterImpl userAdapter;
    
    private UserModel user;
    private UserModel userWithoutId;
    private UserInfoEntity entity;
    private UserInfoEntity entityWithoutBirthdate;
    private Iterable<UserInfoEntity> entities;
    
    /**
     * Sets up test environment.
     * 
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception{
        super.setUp();
        user = buildDummyUserModel(ANY_INT_AS_STRING);
        userWithoutId = buildDummyUserModel(null);
        entity = buildDummyUserEInfoEntity(ANY_SQL_DATE);
        entityWithoutBirthdate = buildDummyUserEInfoEntity(null);
        entities = buildDummyEntitiesList();
    }
    
    /**
     * Tests {@link UserAdapterImpl#adaptModelToEntity(UserModel)}
     */
    @Test
    public void adaptModelToEntityTest(){
        
        UserInfoEntity entity = userAdapter.adaptModelToEntity(user);
        
        Assert.assertNotNull(entity);
        Assert.assertEquals(ANY_INT, entity.getId().intValue());
    }
    
    /**
     * Tests {@link UserAdapterImpl#adaptModelToEntity(UserModel)}
     */
    @Test
    public void adaptModelToEntityWithNullIdTest(){
        
        UserInfoEntity entity = userAdapter.adaptModelToEntity(userWithoutId);
        
        Assert.assertNotNull(entity);
        Assert.assertNull(entity.getId());
    }
    
    /**
     * Tests {@link UserAdapterImpl#adaptEntityToModel(UserInfoEntity)}
     */
    @Test
    public void adaptEntityToModelTest(){
        
        UserModel model = userAdapter.adaptEntityToModel(entity);
        
        Assert.assertNotNull(model);
        Assert.assertEquals(new StringBuilder().append(entity.getId()).toString(), model.getId());
    }
    
    /**
     * Tests {@link UserAdapterImpl#adaptEntityToModel(UserInfoEntity)}
     */
    @Test
    public void adaptEntityWithoutBirthdateToModelTest(){
        
        UserModel model = userAdapter.adaptEntityToModel(entityWithoutBirthdate);
        
        Assert.assertNotNull(model);
        Assert.assertEquals(new StringBuilder().append(entity.getId()).toString(), model.getId());
    }
    
    /**
     * Tests {@link UserAdapterImpl#adaptEntitiesToModel(Iterable)}
     */
    @Test
    public void adaptEntitiesToModel(){
        
        List<UserModel> model = userAdapter.adaptEntitiesToModel(entities);
        
        Assert.assertNotNull(model);
        Assert.assertTrue(entities.iterator().hasNext());
    }
    
}
