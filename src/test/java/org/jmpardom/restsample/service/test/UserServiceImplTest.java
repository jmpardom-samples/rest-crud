package org.jmpardom.restsample.service.test;

import java.util.ArrayList;
import java.util.List;

import org.jmpardom.common.AbstractUnitaryTest;
import org.jmpardom.common.exception.InvalidParameterException;
import org.jmpardom.common.exception.NoResultsFoundException;
import org.jmpardom.restsample.component.UserAdapter;
import org.jmpardom.restsample.entity.UserInfoEntity;
import org.jmpardom.restsample.model.UserModel;
import org.jmpardom.restsample.repository.UserRepository;
import org.jmpardom.restsample.service.impl.UserServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

/**
 * Mocked jUnit test for {@link UserServiceImpl}
 * 
 * @author jmpardom
 */
@SuppressWarnings("unchecked")
public class UserServiceImplTest extends AbstractUnitaryTest {
    
    @InjectMocks
    private UserServiceImpl userService;
    
    @Mock
    private UserRepository userRepository;
    
    @Mock
    private UserAdapter userAdapter;
    
    private UserModel user;
    private UserModel userWithoutId;
    private List<UserModel> users;
    private UserInfoEntity entity;
    private Iterable<UserInfoEntity> entities;
    private Iterable<UserInfoEntity> entitiesEnmptyList;
    
    /**
     * Sets up test environment.
     * 
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception{
        super.setUp();
        user = buildDummyUserModel(ANY_INT_AS_STRING);
        userWithoutId = buildDummyUserModel(null);
        entity = buildDummyUserEInfoEntity(ANY_SQL_DATE);
        entities = buildDummyEntitiesList();
        entitiesEnmptyList = new ArrayList<UserInfoEntity>();
    }
    
    /**
     * Tests {@link UserServiceImpl#getAllUsers()}
     */
    @Test
    public void getAllUsersTest(){
        
        Mockito.when(userRepository.findAll()).thenReturn(entities);
        Mockito.when(userAdapter.adaptEntitiesToModel(Mockito.any(Iterable.class))).thenReturn(users);
        
        userService.getAllUsers();
        
        Mockito.verify(userRepository).findAll();
        Mockito.verify(userAdapter).adaptEntitiesToModel(Mockito.any(Iterable.class));
        Mockito.verifyNoMoreInteractions(userRepository, userAdapter);
    }
    
    /**
     * Tests {@link UserServiceImpl#getAllUsers()}
     */
    @Test(expected = NoResultsFoundException.class)
    public void getAllUsersNullResultTest(){
        
        Mockito.when(userRepository.findAll()).thenReturn(null);
        
        userService.getAllUsers();
        
        Mockito.verify(userRepository).findAll();
        Mockito.verifyNoMoreInteractions(userRepository);
        Mockito.verifyZeroInteractions(userAdapter);
    }
    
    /**
     * Tests {@link UserServiceImpl#getAllUsers()}
     */
    @Test(expected = NoResultsFoundException.class)
    public void getAllUsersEmptyResultsTest(){
        
        Mockito.when(userRepository.findAll()).thenReturn(entitiesEnmptyList);
        
        userService.getAllUsers();
        
        Mockito.verify(userRepository).findAll();
        Mockito.verifyNoMoreInteractions(userRepository);
        Mockito.verifyZeroInteractions(userAdapter);
    }
    
    /**
     * Tests {@link UserServiceImpl#getUser(String)}
     */
    @Test
    public void getUserTest(){
        
        Mockito.when(userRepository.findOne(Mockito.anyInt())).thenReturn(entity);
        Mockito.when(userAdapter.adaptEntityToModel(Mockito.any(UserInfoEntity.class))).thenReturn(user);
        
        userService.getUser(ANY_INT_AS_STRING);
        
        Mockito.verify(userRepository).findOne(Mockito.anyInt());
        Mockito.verify(userAdapter).adaptEntityToModel(Mockito.any(UserInfoEntity.class));
        Mockito.verifyNoMoreInteractions(userRepository, userAdapter);
    }
    
    /**
     * Tests {@link UserServiceImpl#getUser(String)}
     */
    @Test(expected = InvalidParameterException.class)
    public void getUserNullResultTest(){
        
        Mockito.when(userRepository.findOne(Mockito.anyInt())).thenReturn(null);
        
        userService.getUser(ANY_INT_AS_STRING);
        
        Mockito.verify(userRepository).findAll();
        Mockito.verifyNoMoreInteractions(userRepository);
        Mockito.verifyZeroInteractions(userAdapter);
    }
    
    /**
     * Tests {@link UserServiceImpl#addUser(UserModel)}
     */
    @Test
    public void addUserTest(){
        
        Mockito.when(userRepository.save(Mockito.any(UserInfoEntity.class))).thenReturn(entity);
        Mockito.when(userAdapter.adaptModelToEntity(Mockito.any(UserModel.class))).thenReturn(entity);
        Mockito.when(userAdapter.adaptEntityToModel(Mockito.any(UserInfoEntity.class))).thenReturn(user);
        
        userService.addUser(userWithoutId);
        
        Mockito.verify(userRepository).save(Mockito.any(UserInfoEntity.class));
        Mockito.verify(userAdapter).adaptModelToEntity(Mockito.any(UserModel.class));
        Mockito.verify(userAdapter).adaptEntityToModel(Mockito.any(UserInfoEntity.class));
        Mockito.verifyNoMoreInteractions(userRepository, userAdapter);
    }
    
    /**
     * Tests {@link UserServiceImpl#updateUser(UserModel)}
     */
    @Test
    public void updateUserTest(){
        
        Mockito.when(userRepository.findOne(Mockito.anyInt())).thenReturn(entity);
        Mockito.when(userRepository.save(Mockito.any(UserInfoEntity.class))).thenReturn(entity);
        Mockito.when(userAdapter.adaptModelToEntity(Mockito.any(UserModel.class))).thenReturn(entity);
        Mockito.when(userAdapter.adaptEntityToModel(Mockito.any(UserInfoEntity.class))).thenReturn(user);
        
        userService.updateUser(user);
        
        Mockito.verify(userRepository).findOne(Mockito.anyInt());
        Mockito.verify(userRepository).save(Mockito.any(UserInfoEntity.class));
        Mockito.verify(userAdapter).adaptModelToEntity(Mockito.any(UserModel.class));
        Mockito.verify(userAdapter).adaptEntityToModel(Mockito.any(UserInfoEntity.class));
        Mockito.verifyNoMoreInteractions(userRepository, userAdapter);
    }
    
    /**
     * Tests {@link UserServiceImpl#updateUser(UserModel)}
     */
    @Test(expected = InvalidParameterException.class)
    public void updateNotExistingUserTest(){
        
        Mockito.when(userRepository.findOne(Mockito.anyInt())).thenReturn(null);
        
        userService.updateUser(user);
        
        Mockito.verify(userRepository).findOne(Mockito.anyInt());
        Mockito.verifyNoMoreInteractions(userRepository);
        Mockito.verifyZeroInteractions(userAdapter);
    }
    
    /**
     * Tests {@link UserServiceImpl#deleteUser(String)}
     */
    @Test
    public void deleteUserTest(){
        
        Mockito.when(userRepository.findOne(Mockito.anyInt())).thenReturn(entity);
        
        userService.deleteUser(ANY_INT_AS_STRING);
        
        Mockito.verify(userRepository).findOne(Mockito.anyInt());
        Mockito.verify(userRepository).delete(Mockito.anyInt());
        Mockito.verifyNoMoreInteractions(userRepository);
        Mockito.verifyZeroInteractions(userAdapter);
    }
    
    /**
     * Tests {@link UserServiceImpl#deleteUser(String)}
     */
    @Test(expected = InvalidParameterException.class)
    public void deleteNotExistingUserTest(){
        
        Mockito.when(userRepository.findOne(Mockito.anyInt())).thenReturn(null);
        
        userService.deleteUser(ANY_INT_AS_STRING);
        
        Mockito.verify(userRepository).findOne(Mockito.anyInt());
        Mockito.verifyNoMoreInteractions(userRepository);
        Mockito.verifyZeroInteractions(userAdapter);
    }
    
}
