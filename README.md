# REST exercise

You have to develop a rest web service for an IUD (AKA CRUD) example application.

There is no any special requirement except do it in a semantic way.

Other optional and desirable features (choose those ones which you makes you feel comfortable):

* HTTP verbs
* Entity validation
* Global exception handling
* Custom HTTP error codes
* Unit testing
* Asynchronous Programming
* Dependency Management (maven, gradle or nuget)
* Connect it to a database (in memory or localdb)

Suggestions

* Feel free to use any framework or third party library you like. If you feel it is not the obvious choice, please justify yourself.
* Small files and semantic folder structure will be appreciated. Keeping the Single Responsibility of classes will make you look extraordinary.
* We like annotations if you are developing Java. We love dependency injection in any case.
* We know is a very stupid simple app (it’s an exercise), but work thinking it’s the beginning of a larger project. Some elements have no sense for the IUD requirement (separation in different namespaces/packages, etc.) but it would be great if anyone want to add the IUD for another entity.
* We are very (VERY) strict with naming and conventions (the “other” hard problem). There is no Good Convention, just choose one and keep it coherent. Code in English, this is the main reason we send you this in English.
* Publish your project in a public repository or in a public free web service

# REST exercise solution

## Overview

We have built a 3 level RESTFULL compliant system that executes basic CRUD operations with users. The systems satisfy all  RESTFULL levels except HATEOAS. The system:

* __Hypermedia Contols:__ Uses REST call sHTTP protocol
* __HTTP Verbs:__ Uses POST, PUT, DELETE or GET HTTP method depending on the operation which will be executed
* __Oriented to ersources:__ At this point, we have changed the endpoits definition to match this RESTFULL level
* __The Swamp of POX:__ The system is not compliant with HATEOAS, however it can be easily added using spring hateoas module   

```xml
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-hateoas</artifactId>
        </dependency>
```

## Frameworks & Technologies

* __Language:__ JAVA 8
* __Maven:__ Maven 3. Dependency management
* __Spring boot:__ 1.5.3 RELEASE
* __Testing:__ junit, spring test, mockito, meanbean
* __DB:__ In memory DB H2


## API


user.json 
```json
{ 
  	"id": "string",
  	"name" : "string",
  	"birthdate" : "string"
   
} 
```

users.json
```json
{ 
  	"id": "string",
  	"name" : "string",
  	"birthdate" : "string"
   
},
{ 
  	"id": "string",
  	"name" : "string",
  	"birthdate" : "string"
   
} 
,
...
```


errors.json

```json
{
  "errors": [
    {
      "code": "string",
      "message": "string",
      "level": "ERROR|FATAL|INFO|WARNING",
      "description": "string"
    }
  ]
}
```

_get all_
* url: /server/api/user
* method: GET
* params: <none>
* returns: a users.json elements

_get one_
* url: /server/api/user/{id}
* method: GET
* params: id (int) or errors.json
* returns: user.json or errors.json

_create_
* url: /server/api/user
* method: POST
* params: user.json (ignores id and generates a new one) 
* returns: user.json (with generated id) HTTP 201 on success

_update_
* url: /server/api/user
* method: PUT
* params: user.json or errors.json
* returns: user.json HTTP 200 on success

_remove_
* url: /server/api/user/{id}
* method: DELETE
* params: id (int) or errors.json
* returns: <void> HTTP 204 on success

_Notice that we have changed some endpoints in order to enable resources discovery principle_

## Satisfying Requirements

* __HTTP verbs__: The system uses POST, PUT, GET or DELETE in the right way (see above explanations), besides, it handles the correct HTTP code depending on the operation type.
* __Entity validation__: The system uses javax.validation  package and annotations to validate not null elements, size of elements or date format (yyyy-mm-dd)
* __Global exception handling__: Systems enables a control advice to handle exceptions just at one point (`ExceptionHandlerAdvice.java`) and overrides spring boot error controller to handle HTTP 404 or any different exception which does not extends from `java.lang.Exception` (`CustomErrorController.java`). In case of operation failure the systems builds automatically a response with the error list using the `ErrorManager.java` and the configuration specified in the `application-error.properties` file.
* __Custom HTTP error codes__:The system handles  the correct HTTP code depending on the operation or returns a error structure when the operation fails
* __Unit testing__:We use spirng test, mockito, and meanbean for testing. Tests are also integrated with in memory database H2
* __Asynchronous Programming__: We actually think that in this class of application it is not properly to include asynchronous features, however we have provided an  asynchronous service calls using `java.util.concurrent` package and spring features such as `@Async` or `Future` 
* __Dependency Management (maven, gradle or nuget)__:Using maven
* __Connect it to a database (in memory or localdb)__:Using H2


__All application has been tested with postman and has around 95% of coverage__

